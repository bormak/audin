"use strict"

var $ = require('jquery'),
    _ = require('lodash'),
    fs = require('fs'),
    path = require('path'),
    lib = require('./lib');
  

module.exports = {
  sel: false,
  search: false,
  paused: false,
  colors: {
    track: 'brown',
    alb: 'green'
  },
  arts: lib.load(),

  current: function() {
    var lines = lib.current(),
        rest = lines[0];
    
    if (rest == 'No song playing.') {
      if (!lib.exec('pgrep audacious'))
        lib.start();
      else {
        $('#state').empty().append('Silence');
        if (this.albs && this.alb)
          this.playNext()             
      }
      return;
    }

    if (!self.paused)
      $('#state').empty()

    _.each(['track', 'alb', 'art'], _.bind(function(key) {
      var name = path.basename(rest);      
      rest = path.dirname(rest);
      
      if (this[key] != name && !this.sel && !this.search) {
        $('#' + key).empty().append(name);
        if (key != 'art') {
          this.fill(key, rest);
          this.changeColors(key, name);
        } 
        this[key] = name;
      }
    }, this));

    _.each(['#length', '#played', '#bitrate'], function(id, n) {
      $(id).empty().append(lines[n + 1]);
    });
  },

  keyhandler: function() {
    var self = this,
        fmap = {
          w: function() {
            if (self.sel) {
              lib.empty(['arts', 'albs']);
              self.art = self.alb = self.track = null;
            }
            else
              self.showArtists();
            self.sel = !self.sel; 
          },
          p: function() {
              var cmd = lib.audtool('playback-status') == "playing\n" ? 'pause' : 'play';   
              lib.audtool('playback-' + cmd);
              $('#state').empty()
              if (cmd == 'pause')
                $('#state').append('Paused');
              self.paused = !self.paused;
          },
          z: function() {
            if (self.search) 
              $('#search').empty();
            else
              lib.search(self);
            self.search  = !self.search;
          },
        };

    return function(e) {
      if (e.ctrlKey) {
        var key = String.fromCharCode(96 + e.which);
        if (_.has(fmap, key)) {
          fmap[key]();
          e.preventDefault();
        }
      }
    };
  },

  showArtists: function() {
    var self = this;
    lib.empty(['albs', 'tracks', 'search']);
    this.arts = lib.load();
    $('#arts').append("<p></p>");
    _.each(_.keys(this.arts).sort(), function(art, n) {
      $('#arts').append(lib.button('art' + (n+1), self.arts[art], 20));
    });

    $('#arts button').click(function(e) {
      self.selectArtist($(this)[0].name);
      return false;
    });
  },

  showAlbums: function(art) {
    var self = this;
    
    $('#albs').append("<p></p>", path.basename(art) + ': ');

    _.each(this.albs, function(alb, n) {
      $('#albs').append(lib.button('alb' + (n+1), path.join(art, alb), 40));
    });

    $('#albs button').click(function(e) {
      self.playAlbum($(this)[0].name);
      return false;
    });
  },

  selectArtist: function(art) {
    var nodirs;

    lib.empty(['albs']);
    this.albs = lib.sort(fs.readdirSync(art));
    nodirs = _.every(this.albs, function(alb) {
      return fs.statSync(path.join(art, alb)).isFile()
    });
    // check if albs has at least one directory(album)
    
    // if not, play it art is also album
    if (nodirs) {
      this.playAlbum(art);
      return;
    }
        
    if (this.albs.length > 1) {
      this.showAlbums(art);
    }
    else
      this.playAlbum(path.join(art, this.albs[0]));
  },

  playAlbum: function(alb) {
    $('#tracks').empty();
    if (fs.statSync(alb).isFile())
      lib.play([alb])
    else
      lib.play(fs.readdirSync(alb), alb);
    
    lib.empty(['arts', 'search']);
    this.sel = this.search = false;
  },

  playNext: function() {
    var last = _.indexOf(this.albs, this.alb),
        next = last + 1;
    if (next < this.albs.length) {
    	this.playAlbum(path.join(this.arts[this.art], this.albs[next]));
      $('#alb' + last).css('color', 'green');
      $('#alb' + next).css('color', 'red');
    }
    else
      lib.empty(['alb', 'track', 'length', 'played']);
  },

  fill: function(sing, dir) {
    var pl = sing + 's',
        isArt = _.includes(_.values(this.arts), dir);

    if (sing == 'track' && isArt)
    	return;
       
    if ($("#" + pl + " > button").length == 0 && !this.sel && !this.search) {
      this[pl] = fs.readdirSync(dir);
      if (sing == 'track')
        lib.showTracks(this.tracks)
      else if (isArt) 
        this.showAlbums(dir);
    }
  },

  changeColors: function(key, name) {
    if (this[key + 's']) {
      if (this[key])
        this.color(key, this[key], this.colors[key]);
      this.color(key, name, 'red');
    }
  },

  color: function(key, name, color) {
    var index = _.indexOf(this[key + 's'], name) + 1;
    $('#' + key + index).css("color", color);
  },

}

