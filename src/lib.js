"use strict"

var lib = exports,
    cproc = require('child_process'),
    $ = require('jquery'),
    _ = require('lodash'),
    format = require('util').format,
    fs = require('fs'),
    path = require('path'),
    ext = /\.mp3$|\.mp4a$|\.mpc$|\.ogg$/;

lib.exec = function(cmd) {
  try {
    return cproc.execSync(cmd).toString();
  }
  catch (err) {
    return null;
  }
};

lib.audtool = function(cmd) {
    return this.exec('audtool ' + cmd);
};

lib.current = function() {
  var cmd = '%filename %length %output-length %bitrate-kbps'
      .replace(/%/g, 'current-song-');
  return this.audtool(cmd).split(/\n/);
};

lib.start = function() {
   $('#state').empty().append('Starting audacious');
  cproc.exec('audacious -h &');
};

lib.play = function(files, alb) {
  var tracks = _.filter(files, function(file) {
    return ext.test(file);
  });

  lib.audtool('playlist-clear'); 
  
  _.each(tracks, function(track) {
    var cmd = "playlist-addurl \"";
    if (alb)
      cmd += alb + "/";
    cmd += track + "\"";
    lib.audtool(cmd);
  });
  
  lib.audtool('playback-play');   
};


lib.button = function(id, name, len) {
  var title = path.basename(name)
      .replace(ext,'')
      .replace('.', '')
      .substring(0, len);
  return format('<button id="%s" name="%s">%s</button>', id, name, title);
};


lib.empty = function(divs) {
  _.each(divs, function(div) { 
    $('#' + div).empty(); 
  });
};

lib.showTrack = function(num, track) {
  $('#tracks').append(this.button('track' + (num + 1), track, 50));
};

lib.showTracks = function(tracks) {
  $('#tracks').append("<p></p>");
  _.each(tracks, _.bind(function(track, num) {
    if (track.match(ext))
      this.showTrack(num, track);
  }, this));

  $('#tracks button').click(function(e) {
    lib.audtool('playlist-jump ' + $(this)[0].id.replace('track',''));
    return false;
  });  
};

lib.sort = function(albs) {
  return _.sortBy(albs, function(alb) {
    var year, added, re = /^\d{2}[\s+|_|-]/;
    if (alb.substr(0,2) == 'M0')
      return alb.replace('M0', '200');
    year = alb.match(re);
    if (year) {
      added = year[0].substr(0,2) < 30 ? '20' + alb : '19' + alb;
      return added;
    }
    // works until 2030 
    return alb;
  });
};

lib.load = function() {
  var buf = fs.readFileSync(path.join(process.env['HOME'],'.nwaud'), 'utf8'),
      roots = buf.replace(/\n+/,'').split(/\s+/);

  return function() {
    var arts = {};
    // add only new keys 
    _.each(roots, function(dir, n) {
      if (dir.length > 0)
        _.each(fs.readdirSync(dir), function(name) {
          arts[name] = path.join(dir, name) 
        });   
    });
    return arts;
  };
}();

lib.search = function(player) {
  var arts = player.arts;

  $("#arts").empty();
  $("#search").append("<p></p>", '<input type="text" id="artsel" />');
  $("#artsel").autocomplete({
    source: function(request, response) {
      var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex(request.term), "i");
      response($.grep(_.keys(arts), function(art) {
        return matcher.test(art);
      }))
    },

    select: function(e, ui) {
      player.selectArtist(arts[ui.item.label]);
    },

    messages: {
      noResults: '',
      results: function() {}
    },
  });

  $("#artsel").trigger("focus");
  $("#artsel").val(null);
};


  
